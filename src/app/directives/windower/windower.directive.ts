import {IAugmentedJQuery, IDirective, IDirectiveFactory, IScope} from "angular";

export class WindowerDirective implements IDirective {
    static selector = 'kendoWindower';
    restrict = 'A';

    static factory(): IDirectiveFactory {
        const directive = () => new WindowerDirective();
        directive.$inject = [] as any[];
        return directive;
    }

    constructor() {}

    link = (scope: IScope, element: IAugmentedJQuery) => {
        console.log("windower directive created...");
    }
}