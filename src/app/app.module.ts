import { module } from 'angular';
import Root from "./components/root.component";
import {WindowerDirective} from "./directives/windower/windower.directive";
import {FirstComponent} from "./components/first/first.component";


export const moduleName = module('application', [ "kendo.directives" ])
    .directive(WindowerDirective.selector, WindowerDirective.factory())
    .component(FirstComponent.selector, FirstComponent)
    .component(Root.selector, Root).name;
